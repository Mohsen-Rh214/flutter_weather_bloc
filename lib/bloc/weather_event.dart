import 'package:equatable/equatable.dart';

abstract class WeatherEvent extends Equatable {
  WeatherEvent();
}

class FetchWeather extends WeatherEvent {
  final String? cityName;
  final double? latitude;
  final double? longitude;

  FetchWeather({this.cityName, this.latitude, this.longitude});

  @override
  List<Object?> get props => [cityName, longitude, latitude];
}
