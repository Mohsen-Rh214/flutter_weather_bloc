import 'package:equatable/equatable.dart';
import 'package:flutter_weather_bloc/model/weather.dart';

class WeatherState extends Equatable {
  WeatherState();

  @override
  List<Object?> get props => [];
}

class WeatherEmpty extends WeatherState {}

class WeatherLoading extends WeatherState {}

class WeatherLoaded extends WeatherState {
  final Weather weather;

  WeatherLoaded({required this.weather});

  @override
  List<Object?> get props => [weather];
}

class WeatherError extends WeatherState {
  final int errorCode;

  WeatherError({required this.errorCode}) : assert(errorCode != null);

  @override
  List<Object?> get props => [errorCode];
}
