import 'package:bloc/bloc.dart';
import 'package:flutter_weather_bloc/api/http_exception.dart';
import 'package:flutter_weather_bloc/bloc/weather_event.dart';
import 'package:flutter_weather_bloc/bloc/weather_state.dart';
import 'package:flutter_weather_bloc/model/weather.dart';
import 'package:flutter_weather_bloc/repository/weather_repository.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final WeatherRepository weatherRepository;

  WeatherBloc({required this.weatherRepository}) : super(WeatherEmpty()) {
    on<FetchWeather>((event, emit) async {
      emit(WeatherLoading());
      try {
        final Weather weather = await weatherRepository.getWeather(
          event.cityName,
          latitude: event.latitude,
          longitude: event.longitude,
        );
        emit(WeatherLoaded(weather: weather));
      } catch (exception) {
        print(exception);
        if (exception is HttpException) {
          emit(WeatherError(errorCode: exception.code));
        } else
          emit(WeatherError(errorCode: 500));
      }
    });
  }
}
