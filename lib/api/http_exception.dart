/// represents an error state from an network API request
/// [code] represents the HTTP response code
/// string [message] reason if any
class HttpException implements Exception {
  final int code;
  final String message;

  HttpException(this.code, this.message);
}
