import 'package:flutter/material.dart';
import '../main.dart';
import 'empty_widget.dart';

/// General utility widget used to render a cell divided into three rows
/// First row displays [label]
/// second row displays [iconData]
/// third row displays [value]
class ValueTile extends StatelessWidget {
  final String label;
  final String value;
  final IconData iconData;

  ValueTile(this.label, this.value, {this.iconData = Icons.error});

  @override
  Widget build(BuildContext context) {
    ThemeData appTheme = AppStateContainer.of(context).theme;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          this.label,
          style: TextStyle(fontSize: 13,
              color: appTheme.colorScheme.secondary.withAlpha(80)),
        ),
        SizedBox(height: 5),
        this.iconData != Icons.error
            ? Icon(
                iconData,
                color: appTheme.colorScheme.secondary,
                size: 20,
              )
            : EmptyWidget(),
        SizedBox(height: 10),
        Text(
          this.value,
          style: TextStyle(color: appTheme.colorScheme.secondary),
        ),
      ],
    );
  }
}
