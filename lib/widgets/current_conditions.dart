import 'package:flutter/material.dart';
import 'package:flutter_weather_bloc/main.dart';
import 'package:flutter_weather_bloc/model/weather.dart';
import 'package:flutter_weather_bloc/utils/converters.dart';
import 'package:flutter_weather_bloc/widgets/value_tile.dart';

/// Renders Weather Icon, current, min and max temperatures
class CurrentConditions extends StatelessWidget {
  final Weather weather;
  const CurrentConditions({Key? key, required this.weather}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData appTheme = AppStateContainer.of(context).theme;
    TemperatureUnit unit = AppStateContainer.of(context).temperatureUnit;

    int currentTemp = this.weather.temperature.as(unit).round();
    int maxTemp = this.weather.maxTemperature.as(unit).round();
    int minTemp = this.weather.minTemperature.as(unit).round();

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          weather.getIconData(),
          color: appTheme.colorScheme.secondary,
          size: 70,
        ),
        SizedBox(height: 20),
        Text(
          '$currentTemp°',
          style: TextStyle(
              fontSize: 100,
              fontWeight: FontWeight.w100,
              color: appTheme.colorScheme.secondary),
        ),
        Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          ValueTile("max",
              '$maxTemp°'),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Center(
                child: Container(
              width: 1,
              height: 30,
              color: appTheme.colorScheme.secondary.withAlpha(50),
            )),
          ),
          ValueTile("min",
              '$minTemp°'),
        ]),
      ],
    );
  }
}
