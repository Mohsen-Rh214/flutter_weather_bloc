import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:flutter_weather_bloc/model/weather.dart';
import 'package:flutter_weather_bloc/widgets/temperature_line_chart.dart';
import '../main.dart';
import 'current_conditions.dart';
import 'empty_widget.dart';

class WeatherSwipePager extends StatelessWidget {
  final Weather weather;

  const WeatherSwipePager({Key? key, required this.weather}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData appTheme = AppStateContainer.of(context).theme;

    return Container(
      width: MediaQuery.of(context).size.width,
      height: 300,
      child: Swiper(
        itemCount: 2,
        index: 0,
        itemBuilder: (context, index) {
          if (index == 0) {
            return CurrentConditions(
              weather: weather,
            );
          } else if (index == 1) {
            return TemperatureLineChart(
              weather.forecast,
              animate: true,
            );
          }
          return EmptyWidget();
        },
        pagination: SwiperPagination(
            margin: EdgeInsets.all(5.0),
            builder: DotSwiperPaginationBuilder(
                size: 5,
                activeSize: 5,
                color: appTheme
                    .colorScheme
                    .secondary
                    .withOpacity(0.4),
                activeColor:
                appTheme.colorScheme.secondary)),
      ),
    );
  }
}
