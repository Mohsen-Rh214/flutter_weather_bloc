import 'package:flutter/material.dart';
import 'package:flutter_weather_bloc/main.dart';
import 'package:flutter_weather_bloc/model/weather.dart';
import 'package:flutter_weather_bloc/widgets/forecast_horizontal_widget.dart';
import 'package:flutter_weather_bloc/widgets/value_tile.dart';
import 'package:flutter_weather_bloc/widgets/weather_swipe_pager.dart';
import 'package:intl/intl.dart';

class WeatherWidget extends StatelessWidget {
  final Weather weather;

  WeatherWidget({required this.weather});

  @override
  Widget build(BuildContext context) {
    ThemeData appTheme = AppStateContainer.of(context).theme;

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            weather.cityName.toUpperCase(),
            style: TextStyle(
              fontWeight: FontWeight.w900,
              letterSpacing: 5,
              fontSize: 25,
              color: appTheme.colorScheme.secondary,
            ),
          ),
          SizedBox(height: 20),
          Text(
            weather.description.toUpperCase(),
            style: TextStyle(
              fontWeight: FontWeight.w100,
              letterSpacing: 5,
              fontSize: 15,
              color: appTheme.colorScheme.secondary,
            ),
          ),
          WeatherSwipePager(weather: weather),
          Padding(
              padding: EdgeInsets.all(10),
              child: Divider(
                  color: appTheme
                      .colorScheme
                      .secondary
                      .withAlpha(50))),
          ForecastHorizontal(weathers: weather.forecast),
          Padding(
            padding: EdgeInsets.all(10),
            child: Divider(
              color: AppStateContainer.of(context)
                  .theme
                  .colorScheme
                  .secondary
                  .withAlpha(50),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ValueTile('wind speed', '${weather.windSpeed} m/s'),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Center(
                  child: Container(
                    width: 1,
                    height: 30,
                    color: AppStateContainer.of(context).theme.colorScheme.secondary.withAlpha(50),
                  ),
                ),
              ),
              ValueTile(
                  'sunrise',
                  DateFormat('h:mm a').format(
                      DateTime.fromMillisecondsSinceEpoch(
                          weather.sunrise * 1000))),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Center(
                    child: Container(
                  width: 1,
                  height: 30,
                  color: AppStateContainer.of(context).theme.colorScheme.secondary.withAlpha(50),
                )),
              ),
              ValueTile(
                  'sunset',
                  DateFormat('h:mm a').format(
                      DateTime.fromMillisecondsSinceEpoch(
                          weather.sunset * 1000))),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Center(
                    child: Container(
                  width: 1,
                  height: 30,
                  color: AppStateContainer.of(context)
                      .theme
                      .colorScheme
                      .secondary
                      .withAlpha(50),
                )),
              ),
              ValueTile("humidity", '${weather.humidity}%'),
            ],
          )
        ],
      ),
    );
  }
}
