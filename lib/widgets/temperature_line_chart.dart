import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_weather_bloc/main.dart';
import 'package:flutter_weather_bloc/model/weather.dart';

/// Renders a line chart from forecast data
/// x axis - date
/// y axis - temperature
class TemperatureLineChart extends StatelessWidget {
  final List<Weather> weathers;
  final bool animate;

  TemperatureLineChart(this.weathers, {required this.animate});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: charts.TimeSeriesChart(
        [
          charts.Series<Weather, DateTime>(
            id: '',
            colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
            domainFn: (Weather weather, _) =>
                DateTime.fromMillisecondsSinceEpoch(weather.time * 1000),
            measureFn: (Weather weather, _) => weather.temperature
                .as(AppStateContainer.of(context).temperatureUnit),
            data: weathers,
          )
        ],
        animate: animate,
        animationDuration: Duration(milliseconds: 500),
        primaryMeasureAxis: const charts.NumericAxisSpec(
          tickProviderSpec: charts.BasicNumericTickProviderSpec(
            zeroBound: false,
          ),
        ),
      ),
    );
  }
}
