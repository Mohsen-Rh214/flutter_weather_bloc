import 'package:flutter/material.dart';
import 'package:flutter_weather_bloc/utils/converters.dart';
import 'package:flutter_weather_bloc/utils/weather_icon_mapper.dart';

class Weather {
  int id = 0;
  int time = 0;
  int sunrise = 0;
  int sunset = 0;
  int humidity = 0;

  String description = 'description';
  String iconCode = 'icon';
  String main = 'main';
  String cityName = 'city';

  double windSpeed = 0;

  Temperature temperature = Temperature(0);
  Temperature maxTemperature = Temperature(0);
  Temperature minTemperature = Temperature(0);

  List<Weather> forecast = [];

  Weather({
    required this.id,
    required this.time,
    required this.sunrise,
    required this.sunset,
    required this.humidity,
    required this.description,
    required this.iconCode,
    required this.main,
    required this.cityName,
    required this.windSpeed,
    required this.temperature,
    required this.maxTemperature,
    required this.minTemperature,
  });

  Weather.b({required this.time, required this.temperature, required this.iconCode});

  static Weather fromJson(Map<String, dynamic> json) {
    final weather = json['weather'][0];
    return Weather(
      id: weather['id'],
      time: json['dt'],

      sunrise: json['sys']['sunrise'],
      sunset: json['sys']['sunset'],
      humidity: json['main']['humidity'],

      description: weather['description'],
      iconCode: weather['icon'],
      main: weather['main'],
      cityName: json['name'],

      windSpeed: intToDouble(json['wind']['speed']),

      temperature: Temperature(intToDouble(json['main']['temp'])),
      maxTemperature: Temperature(intToDouble(json['main']['temp_max'])),
      minTemperature: Temperature(intToDouble(json['main']['temp_min'])),
    );
  }

  static List<Weather> fromForecastJson(Map<String, dynamic> json) {
    final weathers = <Weather>[];
    for (final item in json['list']) {
      weathers.add(
        Weather.b(
          time: item['dt'],
          temperature: Temperature(
            intToDouble(
              item['main']['temp']
            )
          ),
          iconCode: item['weather'][0]['icon'],
        ),
      );
    }
    return weathers;
  }

  IconData getIconData(){
    switch(this.iconCode){
      case '01d': return WeatherIcons.clear_day;
      case '01n': return WeatherIcons.clear_night;
      case '02d': return WeatherIcons.few_clouds_day;
      case '02n': return WeatherIcons.few_clouds_day;
      case '03d':
      case '04d':
        return WeatherIcons.clouds_day;
      case '03n':
      case '04n':
        return WeatherIcons.clear_night;
      case '09d': return WeatherIcons.shower_rain_day;
      case '09n': return WeatherIcons.shower_rain_night;
      case '10d': return WeatherIcons.rain_day;
      case '10n': return WeatherIcons.rain_night;
      case '11d': return WeatherIcons.thunder_storm_day;
      case '11n': return WeatherIcons.thunder_storm_night;
      case '13d': return WeatherIcons.snow_day;
      case '13n': return WeatherIcons.snow_night;
      case '50d': return WeatherIcons.mist_day;
      case '50n': return WeatherIcons.mist_night;
      default: return WeatherIcons.clear_day;
    }
  }
}
