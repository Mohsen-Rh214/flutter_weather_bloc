import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_weather_bloc/api/location.dart';
import 'package:flutter_weather_bloc/bloc/weather_bloc.dart';
import 'package:flutter_weather_bloc/bloc/weather_event.dart';
import 'package:flutter_weather_bloc/bloc/weather_state.dart';
import 'package:flutter_weather_bloc/main.dart';
import 'package:flutter_weather_bloc/widgets/weather_widget.dart';
import 'package:intl/intl.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

enum OptionsMenu { changeCity, settings }

class WeatherScreen extends StatefulWidget {
  @override
  _WeatherScreenState createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen>
    with TickerProviderStateMixin {
  // animation controller
  late AnimationController _fadeController;
  late Animation<double> _fadeAnimation;

  // get weather for the city
  String _cityName = 'Tehran';
  late WeatherBloc _weatherBloc;

  //
  @override
  void initState() {
    super.initState();

    /// weather Controllers
    _fetchWeatherWithLocation();
    _weatherBloc = BlocProvider.of<WeatherBloc>(context);

    /// animation Controllers
    _fadeController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000),
    );
    _fadeAnimation = CurvedAnimation(
      parent: _fadeController,
      curve: Curves.easeIn,
    );
  }

  @override
  Widget build(BuildContext context) {
    ThemeData appTheme = AppStateContainer
        .of(context)
        .theme;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: appTheme.primaryColor,
        elevation: 0,
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              DateFormat('EEEE, d MMMM yyyy').format(DateTime.now()),
              style: TextStyle(
                  color: appTheme.colorScheme.secondary.withAlpha(80),
                  fontSize: 14),
            )
          ],
        ),
        actions: [
          IconButton(
              onPressed: () {
                _fetchWeatherWithLocation();
              },
              icon: Icon(Icons.my_location)),
          PopupMenuButton<OptionsMenu>(
            child: Icon(
              Icons.more_vert,
              color: appTheme.colorScheme.secondary,
            ),
            onSelected: _onOptionMenuItemSelected,
            itemBuilder: (context) =>
            <PopupMenuEntry<OptionsMenu>>[
              const PopupMenuItem<OptionsMenu>(
                value: OptionsMenu.changeCity,
                child: Text('Change City'),
              ),
              const PopupMenuItem<OptionsMenu>(
                value: OptionsMenu.settings,
                child: Text('settings'),
              ),
            ],
          ),
        ],
      ),
      // backgroundColor: Colors.white,
      body: Material(
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: appTheme.primaryColor,
          ),
          child: FadeTransition(
            opacity: _fadeAnimation,
            child: BlocBuilder<WeatherBloc, WeatherState>(
                builder: (_, WeatherState weatherState) {
                  _fadeController.reset();
                  _fadeController.forward();

                  if (weatherState is WeatherLoaded) {
                    _cityName = weatherState.weather.cityName;
                    return WeatherWidget(
                      weather: weatherState.weather,
                    );
                  } else if (weatherState is WeatherError ||
                      weatherState is WeatherEmpty) {
                    String errorText = 'There was an error fetching weather data';
                    if (weatherState is WeatherError &&
                        weatherState.errorCode == 404) {
                      errorText =
                      'We have trouble fetching weather for $_cityName';
                    }
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Icon(
                          Icons.error_outline,
                          color: Colors.redAccent,
                          size: 24,
                        ),
                        SizedBox(height: 10),
                        Text(
                          errorText,
                          style: TextStyle(color: appTheme.colorScheme
                              .secondary),
                        ),
                        TextButton(
                          style: TextButton.styleFrom(
                              primary: appTheme.colorScheme.secondary,
                              elevation: 1),
                          child: Text("Try Again"),
                          onPressed: _fetchWeatherWithCity,
                        )
                      ],
                    );
                  } else if (weatherState is WeatherLoading) {
                    return Center(
                        child: CircularProgressIndicator(
                          backgroundColor: appTheme.primaryColor,
                        ));
                  }
                  return Container(
                    child: Text('No city is set!'),
                  );
                }),
          ),
        ),
      ),
    );
  }

  void _showCityChangeDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text(
              'Change City',
              style: TextStyle(color: Colors.black),
            ),
            actions: [
              OutlinedButton(
                style: OutlinedButton.styleFrom(
                  side: BorderSide(color: Colors.black),
                ),
                child: const Text(
                  'ok',
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
                onPressed: () {
                  _fetchWeatherWithCity();
                  Navigator.of(context).pop();
                },
              )
            ],
            content: TextField(
              autofocus: true,
              onChanged: (text) {
                _cityName = text;
              },
              decoration: const InputDecoration(
                hintText: 'Enter city name to search',
                hintStyle: TextStyle(color: Colors.black),
              ),
              style: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
            ),
          );
        });
  }

  _onOptionMenuItemSelected(OptionsMenu item) {
    switch (item) {
      case OptionsMenu.changeCity:
        _showCityChangeDialog();
        break;
      case OptionsMenu.settings:
        Navigator.of(context).pushNamed("/settings");
        break;
    }
  }

  _fetchWeatherWithCity() {
    _weatherBloc.add(FetchWeather(cityName: _cityName));
  }

  _fetchWeatherWithLocation() async {
    LocationPermission permission;
    await Permission.locationWhenInUse.request();

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        _showLocationDeniedDialog();
        return Future.error('Location permissions are denied');
      }
    }

    Location location = Location();
    await location.getCurrentLocation();

    _weatherBloc.add(FetchWeather(
      longitude: location.longitude,
      latitude: location.latitude,
    ));
  }

  void _showLocationDeniedDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            title: const Text('Location is disabled :(',
                style: TextStyle(color: Colors.black)),
            actions: [
              OutlinedButton(
                style: OutlinedButton.styleFrom(primary: Colors.black),
                child: const Text(
                  'Enable!',
                  style: TextStyle(color: Colors.green, fontSize: 16),
                ),
                onPressed: () {
                  openAppSettings();
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }
}
