import 'package:flutter/material.dart';
import 'package:flutter_weather_bloc/main.dart';
import 'package:flutter_weather_bloc/themes.dart';
import 'package:flutter_weather_bloc/utils/converters.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData appTheme = AppStateContainer.of(context).theme;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: appTheme.primaryColor,
        title: Text('Settings'),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 10, right: 10, top: 15),
        color: appTheme.primaryColor,
        child: ListView(
          children: [
            Padding(
              padding: EdgeInsets.all(8),
              child: Text(
                'Theme',
                style: TextStyle(
                  color:
                  appTheme.colorScheme.secondary,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                ),
                color: appTheme
                    .colorScheme
                    .secondary
                    .withOpacity(0.1),
              ),
              padding: EdgeInsets.only(left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Dark',
                      style: TextStyle(
                        color: appTheme
                            .colorScheme
                            .secondary,
                      )),
                  Radio<dynamic>(
                    value: Themes.DARK_THEME_CODE,
                    groupValue: AppStateContainer.of(context).themeCode,
                    onChanged: (value) {
                      ///updates the theme by selecting radio button
                      AppStateContainer.of(context).updateTheme(value);
                    },
                    activeColor: appTheme
                        .colorScheme
                        .secondary,
                  ),
                ],
              ),
            ),
            Divider(
              color: appTheme.primaryColor,
              height: 1,
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(8),
                  bottomRight: Radius.circular(8),
                ),
                color: appTheme
                    .colorScheme
                    .secondary
                    .withOpacity(0.1),
              ),
              padding: EdgeInsets.only(left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Light',
                      style: TextStyle(
                        color: appTheme
                            .colorScheme
                            .secondary,
                      )),
                  Radio<dynamic>(
                    value: Themes.LIGHT_THEME_CODE,
                    groupValue: AppStateContainer.of(context).themeCode,
                    onChanged: (value) {
                      ///updates the theme by selecting radio button
                      AppStateContainer.of(context).updateTheme(value);
                    },
                    activeColor: appTheme
                        .colorScheme
                        .secondary,
                  ),
                ],
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: 15, left: 8, right: 8, bottom: 8),
              child: Text(
                "Unit",
                style: TextStyle(
                  color:
                  appTheme.colorScheme.secondary,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                ),
                color: appTheme
                    .colorScheme
                    .secondary
                    .withOpacity(0.1),
              ),
              padding: EdgeInsets.only(left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Celsius",
                    style: TextStyle(
                        color: appTheme.colorScheme.secondary),
                  ),
                  Radio<dynamic>(
                    value: TemperatureUnit.celsius.index,
                    groupValue:
                    AppStateContainer.of(context).temperatureUnit.index,
                    onChanged: (value) {
                      AppStateContainer.of(context)
                          .updateTemperatureUnit(TemperatureUnit.values[value]);
                    },
                    activeColor:
                    appTheme.colorScheme.secondary,
                  )
                ],
              ),
            ),
            Divider(
              color: appTheme.primaryColor,
              height: 1,
            ),
            Container(
              color: appTheme
                  .colorScheme.secondary
                  .withOpacity(0.1),
              padding: EdgeInsets.only(left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Fahrenheit",
                    style: TextStyle(
                        color: appTheme.colorScheme.secondary),
                  ),
                  Radio<dynamic>(
                    value: TemperatureUnit.fahrenheit.index,
                    groupValue:
                    AppStateContainer.of(context).temperatureUnit.index,
                    onChanged: (value) {
                      AppStateContainer.of(context)
                          .updateTemperatureUnit(TemperatureUnit.values[value]);
                    },
                    activeColor:
                    appTheme.colorScheme.secondary,
                  ),
                ],
              ),
            ),
            Divider(
              color: appTheme.primaryColor,
              height: 1,
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8)),
                color: appTheme
                    .colorScheme.secondary
                    .withOpacity(0.1),
              ),
              padding: EdgeInsets.only(left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Kelvin",
                    style: TextStyle(
                        color: appTheme.colorScheme.secondary),
                  ),
                  Radio(
                    value: TemperatureUnit.kelvin.index,
                    groupValue:
                    AppStateContainer.of(context).temperatureUnit.index,
                    onChanged: (value) {
                      AppStateContainer.of(context)
                          .updateTemperatureUnit(TemperatureUnit.values[value as int]);
                    },
                    activeColor:
                    appTheme.colorScheme.secondary,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
