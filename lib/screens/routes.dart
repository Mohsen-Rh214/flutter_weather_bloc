import 'package:flutter/cupertino.dart';
import 'package:flutter_weather_bloc/screens/settings_screen.dart';
import 'package:flutter_weather_bloc/screens/weather_screen.dart';

class Routes {

  static final mainRoute = <String, WidgetBuilder>{
    '/home': (context) => WeatherScreen(),
    '/settings': (context) => SettingsScreen(),
  };
}