import 'package:flutter_weather_bloc/api/weather_api_client.dart';
import 'package:flutter_weather_bloc/model/weather.dart';

class WeatherRepository {
  final WeatherApiClient weatherApiClient;

  WeatherRepository({required this.weatherApiClient});

  Future<Weather> getWeather(String? cityName,
      {double? latitude, double? longitude}) async {

    cityName ??= await weatherApiClient.getCityNameFromLocation(
          latitude: latitude, longitude: longitude);

    var weather = await weatherApiClient.getWeatherData(cityName: cityName);
    var weathers = await weatherApiClient.getForecast(cityName: cityName);
    weather.forecast = weathers;

    return weather;
  }
}
